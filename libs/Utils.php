<?php
/**
 * Класс отвечающий за дополнительные утилиты 
 */
namespace libs;


class Utils
{
  
  public static function isAjaxRequest()
	{
		$result = false;
		if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
		{
			$result = true;
		}		
		return $result;
	}
  
  public static function getServerName(){
    return $_SERVER['SERVER_NAME'];
  }
  
  
}