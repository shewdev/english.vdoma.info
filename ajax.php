<? include_once ('db/db.php');
use \libs\Utils;
if(!Utils::isAjaxRequest()){
	  header("Location: //".Utils::getServerName()."/");
    die();
}

if(isset($_POST['answer'])){
    $result =  setPhrasesInDb($_POST);
    echo json_encode($result);

}elseif(isset($_POST['form_name'])){

    if($_POST['form_name']=='folder'){
        $result =  setFolderInDb($_POST['form_input']);
    }elseif($_POST['form_name']=='section'){
        $result =  setSectionInDb($_POST['form_input']);
    }
    echo json_encode($result);

}elseif(isset($_POST['getlist'])){
    $result =  getSectionList($_POST);
    echo json_encode($result);
}elseif(isset($_POST['get_section'])){
    $result = getSections($_POST['folder_id']);
    echo json_encode($result);
}elseif(isset($_POST['section_move'])){
		$result = 0;
	
    if($_POST['section_move'] =='add'){
        unset($_POST['section_move']);
        $result = moveSection($_POST);
        $result =10;
    }elseif($_POST['section_move'] =='delete'){
        $result = deleteSectionFolder($_POST['folder_id'],$_POST['section_id']);
    }

    echo json_encode($result);

}elseif(isset($_POST['action']) && $_POST['action']=='section_remove'){
		$result = removeSectionFolder($_POST);
		echo $result;
}else{
    header("Location: //".Utils::getServerName()."/");
    die();
}
/*
при добавлении слов
после каждого добавления обновляется страница
решение: при добавление слова в раздел не обновлять страницу,
а вызвать метод, который еще раз список перегрузит

нужно выводить сообщение не через alert, а через красивое окошко

если не выбран раздел, нет и кнопки добавить фразу
*/